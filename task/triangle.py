def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    sides = [a, b, c]
    sides.sort()
    if sides[0] + sides[1] > sides[2] and a > 0:
        return True
    return False